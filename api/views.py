from rest_framework.viewsets import ModelViewSet
from .models import Brand, Vehicle, Image, Retailer
from .serializers import BrandSerializer, VehicleSerializer, ImageSerializer, RetailerSerializer


class BrandViewSet(ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    search_fields = ('name', 'country')


class RetailerViewSet(ModelViewSet):
    queryset = Retailer.objects.all()
    serializer_class = RetailerSerializer
    search_fields = ('name', 'latitude', 'longitude', 'rate')


class VehicleViewSet(ModelViewSet):
    queryset = Vehicle.objects.all()
    serializer_class = VehicleSerializer
    search_fields = ('name', 'year', 'price', 'horsepower')


class ImageViewSet(ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    search_fields = ('url', 'description')
