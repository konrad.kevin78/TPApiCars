from django.test import TestCase
from rest_framework import status
from rest_framework.test import APITestCase


class Tests(APITestCase):

    def test_post_retailer(self):

        data = {'name': 'Tesla Motors',
                'latitude': '33.3',
                'longitude': '43.5',
                'rate': -1}

        response = self.client.post('/brands/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_post_brands(self):

        data = {'name': 'Tesla',
                'country': 'USA',
                'vehicles': []}

        response = self.client.post('/brands/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_brand_detail(self):

        data = {'name': 'Tesla',
                'country': 'USA',
                'vehicles': []}

        self.client.post('/brands/', data, format='json')
        response = self.client.get('/brands/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_brands(self):

        data = {'name': 'Tesla',
                'country': 'USA',
                'vehicles': []}

        self.client.post('/brands/', data, format='json')
        response = self.client.get('/brands/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_put_brands(self):

        data = {'name': 'Tesla',
                'country': 'USA',
                'vehicles': []}

        self.client.post('/brands/', data, format='json')

        data = {'name': 'Ferarri',
                'country': 'Italy',
                'vehicles': []}

        response = self.client.put('/brands/1/', data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_brands(self):

        data = {'name': 'Tesla',
                'country': 'USA',
                'vehicles': []}

        self.client.post('/brands/', data, format='json')

        response = self.client.delete('/brands/1/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)