from django.contrib import admin
from api.models import Brand, Vehicle, Retailer, Image

admin.site.register(Brand)
admin.site.register(Vehicle)
admin.site.register(Retailer)
admin.site.register(Image)
