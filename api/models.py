from django.db import models


class Brand(models.Model):
    name = models.CharField(unique=True, max_length=50)
    country = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.name}"


class Retailer(models.Model):
    name = models.CharField(unique=True, max_length=50)
    latitude = models.FloatField()
    longitude = models.FloatField()
    rate = models.FloatField()

    def __str__(self):
        return f"{self.name}"


class Image(models.Model):
    url = models.CharField(max_length=300)
    description = models.CharField(max_length=3000)

    def __str__(self):
        return f"{self.url}"


class Vehicle(models.Model):
    name = models.CharField(unique=True, max_length=50)
    year = models.IntegerField()
    price = models.FloatField()
    horsepower = models.IntegerField()
    retailer = models.ForeignKey(Retailer, related_name='vehicles', on_delete=models.CASCADE)
    brand = models.ForeignKey(Brand, related_name='vehicles', on_delete=models.CASCADE)
    image = models.ForeignKey(Image, related_name='vehicle', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return f"{self.name} {self.year} {self.price} {self.retailer}"
