from django.conf.urls import include, url
from django.contrib import admin
from rest_framework import routers
from api import views

router = routers.DefaultRouter()
router.register(r'brands', views.BrandViewSet)
router.register(r'vehicles', views.VehicleViewSet)
router.register(r'retailers', views.RetailerViewSet)
router.register(r'images', views.ImageViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', admin.site.urls)
]
